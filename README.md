# paheko-container

Paheko on Alpine Linux with Container. With Alpine 3.18, PHP 8.1 and Nginx 1.24.0.

## Usage

Pre-requisite : *Docker Desktop*.

**Option 1** from [*Docker Hub*](https://hub.docker.com/r/deild/paheko) : download the image and launch the container in one line
```
sudo docker run -d -t --name paheko -p 80:80 -v vol-ga:/var/www/paheko/data/ deild/paheko 
```


**Option 2** from the *Github* repository : build the image and launch the container

```
# clone the repository
git clone https://codeberg.org/deild/paheko-container.git
cd paheko-container

# build the image
sudo docker build -t paheko .

# launch the container
sudo docker run -d -t --name paheko -p 8080:80 -v vol-ga:/var/www/paheko/data/ paheko 
```
**Then open [http://localhost:8080](http://localhost:8080) in your browser.**

<hr>

To stop and remove the container
```
sudo docker rm -f paheko
```

To shell inside the container :
```
sudo docker exec -it paheko sh
```

To start off a new database, just remove the volume :
```
sudo docker volume rm vol-ga
```
and run again.

## Publish Docker multi-architecture compliant images

### Login to the container registry

```
sudo docker login codeberg.org
```

### Building your multi-architecture containers

```
sudo docker buildx build \
    --platform linux/amd64,linux/arm64 \
    --tag codeberg.org/deild/paheko:latest \
    --push .
```

## Acknowledgements

This is inspired by [Samuel Allain](https://github.com/SamuelAllain/docker-garradin.git)'s, [HluthWigg](https://github.com/HluthWigg/docker-paheko)'s and [Tim de Pater](https://github.com/TrafeX/docker-php-nginx)'s works. Thanks to them !

It also relies on resources hosted on Paheko's fossil server :

+ [general installation page](https://fossil.kd2.org/paheko/wiki?name=Installation) in French
+ [Debian/Ubuntu setup page](https://fossil.kd2.org/paheko/wiki?name=Installation%20sous%20Debian-Ubuntu)
+ [Nginx configuration](https://fossil.kd2.org/paheko/wiki?name=Installation/nginx)


## More explanations

*Disclaimer : those comments come from a newbie.*

**Paheko** is a PHP-written website, not a simple software.
Therefore, its installation is not that easy.
It requires a web server (Nginx or Apache), with a PHP interpreter (PHP-FPM).

**Supervisord** is used to launch the two required services, Nginx and PHP-FPM, as you can see in *config/supervisord.conf*.
Because the conf file has no "supervisorctl" section (I don't know how to write a working one), it is not possible to use `supervisorctl restart nginx` inside the container.
This would be nice to test new parameters without restarting the whole container. 
Instead I intensively used this command.
```
sudo docker rm -f ga ; sudo docker build -t paheko . ; sudo docker run -d -t --name ga -p 80:80 -v vol-ga:/var/www/paheko/data/ paheko 
```
You can still make sure that both Nginx and PHP-FPM services work with a simple `ps`.
And you can check that your configuration is well taken into account by `nginx -T` (with a lowercase `-t`, it checks the configuration file) and by `php-fpm81 -tt`.

**Nginx and PHP-FPM** communicate through a socket (*/run/paheko.socket*) as you can see in *config/nginx-paheko.conf* and *config/fpm-paheko.conf*.
As I imagine it, when Nginx receive a request over a PHP file, it sends it to PHP for interpretation and PHP hands back a HTML file to Nginx which sends it to the browser.

**Beware :** in the nginx configuration file *paheko.conf*, the root has to be the ***www***, **inside** the *paheko* folder, not the *paheko* itself.
Otherwise, you would receive a warning "Paheko n'est pas installé sur un sous-domaine dédié."

### Version compatibility

If you want to test another Paheko version, you first need to check [available versions of Paheko](https://fossil.kd2.org/paheko/uvlist).
Then you can put it as `PAHEKO_VERSION` in the *Dockerfile*.
If the version you want is only provided with the format *.tar.bz2*, change `tar xzvf paheko-$PAHEKO_VERSION.tar.gz` by `tar xjvf paheko-$PAHEKO_VERSION.tar.bz2`.

Then you have to check which PHP version is required for this Paheko version. You can do that with a case-sensitive "PHP" search on the [changelog](https://fossil.kd2.org/paheko/wiki/?name=Changelog).

Depending on this, you may want a newer or an older Alpine version, so that the PHP version you're looking for is available. Just change `ALPINE_VERSION` in the *Dockerfile*.

**Modules** Make sure you have installed all necessary modules.
For instance, I experienced a crash because the PHP function `finfo_open()`, provided by the module *php81-fileinfo*, was not found.
Indeed the module was not installed.
I added it to the `RUN apk add` line in the **Dockerfile**

### Plugins

(Thanks @gromain) All available plugins as of July 3rd, 2022 in the docker image.
You can install more plugins as explained [here](https://fossil.kd2.org/paheko-plugins/index).
You need to download the `.tar.gz` in the directory `/var/www/paheko/data/plugins` and then connect to paheko (*localhost:80*) > *Configuration* > *Plugins* and then tick and install desired plugins.
**DomPDF** requires **php-zip**.
